package legacy;

import edu.berkeley.cs.jqf.fuzz.Fuzz;
import edu.berkeley.cs.jqf.fuzz.JQF;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(JQF.class)
public class LegacyConverterFuzzer {

    private static LegacyConverter legacyConverter;

    @BeforeClass
    public static void beforeClass(){
        legacyConverter = new LegacyConverter();
    }

    @Fuzz
    public void dollar2euro(Object input){ // They promised that anything goes. Okay!
        try {
            System.out.println("Input: " + input.toString());
            System.out.println("Output: " + legacyConverter.dollar2euro(input));
        } catch (Throwable e) {
            System.out.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Fuzz
    public void fahrenheit2celsius(Double input){ // Fuzzing can be done in a typed manner, too!
        try {
            System.out.println("Input: " + input.toString());
            System.out.println("Output: " + legacyConverter.fahrenheit2celsius(input));
        } catch(Throwable e){
            System.out.println("\"SEE? TOLD YA!\" - your QA dept.");
        }
    }

    @Fuzz
    public void lightyears2kms(Object input){
        try {
            System.out.println(legacyConverter.lightyears2kms(input));
        } catch(Throwable e){
            System.out.println("\"SEE? TOLD YA!\" - your QA dept.");
        }
    }
}

