package legacy;

import java.math.BigDecimal;
import java.math.BigInteger;

public class LegacyConverter {

    public String dollar2euro(Object input){
        BigDecimal inputParsed = new BigDecimal(input.toString());
        BigDecimal dollars = inputParsed.setScale(2, BigDecimal.ROUND_HALF_EVEN);

        BigDecimal multiply = dollars.multiply(BigDecimal.valueOf(0.92));
        BigDecimal euros = multiply.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        return String.valueOf(euros);
    }

    public String fahrenheit2celsius(double fahrenheit){ // For demonstrating typed fuzzing
        return Double.valueOf((fahrenheit - 32.0d) / 1.8).toString();
    }

    public String lightyears2kms(Object lightyears){
        // TODO the.dev: Maybe we should simplify this before shipping to Happy Webapps Inc.
        // Also lightyears could be floating point values, too.
        return BigInteger.valueOf(Long.parseLong(lightyears.toString())).multiply(BigInteger.valueOf(9460753090819L)).toString();
    }
}
