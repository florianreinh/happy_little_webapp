package controllers;

import legacy.LegacyConverter;
import play.mvc.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class ConverterController extends Controller {

    private final LegacyConverter importantConverter;

    public ConverterController(){
        super();
        importantConverter = new LegacyConverter();
    }

    public Result dollar2euro(String dollars) { // String is compatible to dollar2euro(Object).
        return ok(importantConverter.dollar2euro(dollars));
    }

    public Result fahrenheit2celsius(Double fahrenheit) {
        return ok(importantConverter.fahrenheit2celsius(fahrenheit));
    }

    public Result lightyears2kms(Long lightyears) {
        return ok(importantConverter.lightyears2kms(lightyears));
    }

    public Result index() {
        return ok(views.html.index.render());
    }

}
